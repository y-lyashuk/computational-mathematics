#include <iostream>
#include "cubicEquationSolver.h"

int main(int argc, char** argv) {
    if (argc != NO_OF_ARGUMENTS + 1) {
        std::cerr << NO_OF_ARGUMENTS << " arguments required: epsilon, delta, a, b, c" << std::endl;
        return EXIT_FAILURE;
    }

    long double epsilon, delta, equationCoefficients[NO_OF_COEFFICIENTS];

    epsilon = strtold(argv[1], nullptr);
    delta = strtold(argv[2], nullptr);

    equationCoefficients[A_IDX] = strtod(argv[3], nullptr);
    equationCoefficients[B_IDX] = strtod(argv[4], nullptr);
    equationCoefficients[C_IDX] = strtod(argv[5], nullptr);

    std::cout << "Equation: x^3 + (" << equationCoefficients[A_IDX] << ")x^2 + (" << equationCoefficients[B_IDX] << ")x + (" << equationCoefficients[C_IDX] << ")" << std::endl;
    auto roots = solveCubicEquation(epsilon, delta, equationCoefficients);

    std::cout << "Roots:" << std::endl;
    for (int i = 0; i < roots.size(); i++) {
        std::cout << "x" << i << "=" << roots[i] << std::endl;
    }

    return EXIT_SUCCESS;
}
