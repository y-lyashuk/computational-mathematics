#include "cubicEquationSolver.h"

long double f(const long double* coefficients, const long double x) {
    return x * x * x + coefficients[A_IDX] * x * x + coefficients[B_IDX] * x + coefficients[C_IDX];
}

long double bisectionMethod(const long double* coefficients, const std::pair<long double, long double> interval, const long double epsilon) {
    long double a = interval.first, b = interval.second;
    if (std::abs(f(coefficients, a)) < epsilon) {
        return a;
    }
    if (std::abs(f(coefficients, b)) < epsilon) {
        return b;
    }

    long double c, f_c;
    if (f(coefficients, a) < 0 && f(coefficients, b) > 0) {
        while (true) {
            c = (a + b) / 2;
            f_c = f(coefficients, c);

            if (f_c > epsilon) {
                b = c;
                continue;
            }
            if (f_c < -epsilon) {
                a = c;
                continue;
            }

            return c;
        }
    }
    else if(f(coefficients, a) > 0 && f(coefficients, b) < 0) {
        while (true) {
            c = (a + b) / 2;
            f_c = f(coefficients, c);

            if (f_c > epsilon) {
                a = c;
                continue;
            }
            if (f_c < -epsilon) {
                b = c;
                continue;
            }

            return c;
        }
    }
    else {
        std::cerr << "Unexpected results" << std::endl;
        exit(EXIT_FAILURE);
    }
}

std::pair<long double, long double> localizeRoot(const long double* coefficients, long double x1, const long double delta) {
    long double x2 = x1;

    while (f(coefficients, x1) * f(coefficients, x2) > 0) {
        x1 = x2;
        x2 += delta;
    }

    if (x2 > x1) {
        return {x1, x2};
    }
    else {
        return {x2, x1};
    }
}

std::vector<long double> solveQuadraticEquation(const long double* coefficients, const long double epsilon) {
    long double discriminant = coefficients[B_IDX] * coefficients[B_IDX] - 4 * coefficients[A_IDX] * coefficients[C_IDX];
    auto roots = std::vector<long double>();

    if (std::abs(discriminant) < 1e-10) {
        roots.push_back(-coefficients[B_IDX]/2/coefficients[A_IDX]);
    }

    if (discriminant > 1e-10) {
        long double sqrt_discriminant = sqrt(discriminant);

        roots.push_back((-coefficients[B_IDX] - sqrt_discriminant) / 2 / coefficients[A_IDX]);
        roots.push_back((-coefficients[B_IDX] + sqrt_discriminant) / 2 / coefficients[A_IDX]);

        if (roots[0] > roots[1]) {
            std::swap(roots[0], roots[1]);
        }
    }

    return roots;
}

std::vector<long double> calculateExtrema(const long double epsilon, const long double *coefficients) {
    long double derivative[NO_OF_COEFFICIENTS] = {3, 2 * coefficients[A_IDX], coefficients[B_IDX]};

    auto roots = solveQuadraticEquation(derivative, epsilon);

//    for (auto root : roots) {
//        std::cout << root << " ";
//    }
//    std::cout << std::endl;

    return roots;
}

std::vector<long double> solveCubicEquation(const long double epsilon, const long double delta, const long double* coefficients) {
    std::vector<long double> extrema = calculateExtrema(epsilon, coefficients);
    std::vector<long double> roots;

    if (extrema.size() <= 1) {
        if (std::abs(f(coefficients, 0)) < epsilon){
            roots.push_back(0);
        }
        else {
            auto interval = localizeRoot(coefficients, 0, (f(coefficients, 0) > 0) ? -delta : delta);
            roots.push_back(bisectionMethod(coefficients, interval, epsilon));
        }

        return roots;
    }

    long double alpha = extrema[0], f_alpha = f(coefficients, alpha);
    long double beta = extrema[1], f_beta = f(coefficients, beta);

    if (f_alpha < -epsilon && f_beta < -epsilon) {
        auto interval = localizeRoot(coefficients, beta, delta);
        roots.push_back(bisectionMethod(coefficients, interval, epsilon));
    }
    else if (std::abs(f_alpha) < epsilon && f_beta < -epsilon) {
        roots.push_back(alpha);
        auto x2_interval = localizeRoot(coefficients, beta, delta);
        roots.push_back(bisectionMethod(coefficients, x2_interval, epsilon));
    }
    else if (f_alpha > epsilon && f_beta < -epsilon) {
        roots.push_back(bisectionMethod(coefficients, {alpha, beta}, epsilon));

        auto x2_interval = localizeRoot(coefficients, alpha, -delta);
        roots.push_back(bisectionMethod(coefficients, x2_interval, epsilon));

        auto x3_interval = localizeRoot(coefficients, beta, delta);
        roots.push_back(bisectionMethod(coefficients, x3_interval, epsilon));
    }
    else if (f_alpha > epsilon && std::abs(f_beta) < epsilon) {
        auto interval = localizeRoot(coefficients, alpha, -delta);
        roots.push_back(bisectionMethod(coefficients, interval, epsilon));

        roots.push_back(beta);
    }
    else if (f_alpha > epsilon && f_beta > epsilon) {
        auto interval = localizeRoot(coefficients, alpha, -delta);
        roots.push_back(bisectionMethod(coefficients, interval, epsilon));
    }
    else if (std::abs(f_alpha) < epsilon && std::abs(f_beta) < epsilon){
        roots.push_back((alpha + beta)/2);
    }
    else {
        fprintf(stderr, "Unexpected results\n");
        exit(0);
    }

    return roots;
}