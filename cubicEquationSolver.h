#ifndef LAB1_CUBICEQUATIONSOLVER_H
#define LAB1_CUBICEQUATIONSOLVER_H
#include <iostream>
#include <cmath>
#include <vector>

#define NO_OF_ARGUMENTS 5
#define NO_OF_COEFFICIENTS 3

#define A_IDX 0
#define B_IDX 1
#define C_IDX 2

long double f(const long double* coefficients, long double x);
long double bisectionMethod(const long double* coefficients, std::pair<long double, long double> interval, long double epsilon);
std::pair<long double, long double> localizeRoot(const long double* coefficients, long double x1, long double delta);
std::vector<long double> solveQuadraticEquation(const long double* coefficients, long double epsilon);
std::vector<long double> calculateExtrema(long double epsilon, const long double *coefficients);
std::vector<long double> solveCubicEquation(long double epsilon, long double delta, const long double* coefficients);

#endif //LAB1_CUBICEQUATIONSOLVER_H
