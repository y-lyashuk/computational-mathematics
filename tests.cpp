#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "cubicEquationSolver.h"
#include <algorithm>

TEST_CASE() {
    SECTION("(x+1)(x^2+2x+3)=0") {
        long double coefficients[NO_OF_COEFFICIENTS] = {3, 5, 3};
        auto roots = solveCubicEquation(1e-10, 1, coefficients);

        REQUIRE(roots.size() == 1);

        std::cout << "(x+1)(x^2+2x+3)=0" << std::endl;
        for (auto root : roots) {
            std::cout << root << " ";
        }
        std::cout << "\n---------------" << std::endl;
    }
    SECTION("x^3=0") {
        long double coefficients[NO_OF_COEFFICIENTS] = {0, 0, 0};
        auto roots = solveCubicEquation(1e-10, 1, coefficients);

        REQUIRE(roots.size() == 1);

        std::cout << "x^3=0" << std::endl;
        for (auto root : roots) {
            std::cout << root << " ";
        }
        std::cout << "\n---------------" << std::endl;
    }
    SECTION("(x−1)(x^2+2x+3)=0") {
        long double coefficients[NO_OF_COEFFICIENTS] = {1, 1, -3};
        auto roots = solveCubicEquation(1e-10, 1, coefficients);

        REQUIRE(roots.size() == 1);

        std::cout << "(x−1)(x^2+2x+3)=0" << std::endl;
        for (auto root : roots) {
            std::cout << root << " ";
        }
        std::cout << "\n---------------" << std::endl;
    }
    SECTION("x^2(x-3)=0") {
        long double coefficients[NO_OF_COEFFICIENTS] = {-3, 0, 0};

        auto roots = solveCubicEquation(1e-10, 1, coefficients);

        REQUIRE(roots.size() == 2);

        std::cout << "x^2(x-3)=0" << std::endl;
        for (auto root : roots) {
            std::cout << root << " ";
        }
        std::cout << "\n---------------" << std::endl;
    }
    SECTION("(x-5)(x-10)(x-15)=0") {
        long double coefficients[NO_OF_COEFFICIENTS] = {-30, 275, -750};

        auto roots = solveCubicEquation(1e-10, 1, coefficients);

        REQUIRE(roots.size() == 3);

        std::cout << "(x-5)(x-10)(x-15)=0" << std::endl;
        for (auto root : roots) {
            std::cout << root << " ";
        }
        std::cout << "\n---------------" << std::endl;
    }
    SECTION("(x-1)(x-2)^2=0") {
        long double coefficients[NO_OF_COEFFICIENTS] = {-5, 8, -4};

        auto roots = solveCubicEquation(1e-10, 1, coefficients);

        REQUIRE(roots.size() == 2);

        std::cout << "(x-1)(x-2)^2=0" << std::endl;
        for (auto root : roots) {
            std::cout << root << " ";
        }
        std::cout << "\n---------------" << std::endl;
    }
}